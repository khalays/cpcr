<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package origin
 */


$post_id = get_the_ID();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="custom__header" style="background-image:url('<?php echo get_the_post_thumbnail_url($post_id, 'full'); ?>');">
		<div class="main-wrapper">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
		<div class="custom__header__shading"></div>
	</header><!-- .entry-header -->
	<div class="custom__breadcrumbs">
		<div class="main-wrapper">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>
	<div class="entry-content">
		<?php
		if( have_rows('custom') ):
		    while ( have_rows('custom') ) : the_row(); ?>
				<?php if( get_row_layout() == 'content_block' ): ?>
					<div class="main-wrapper">
						<div class="custom__content-block">
							<?php  the_sub_field('content_bloc__contenu'); ?>
						</div>
					</div>
					<?php elseif( get_row_layout() == 'image_block' ):
						$image = get_sub_field('image_block__image');
						// var_dump($image); ?>
						<div class="custom__image-block <?php the_sub_field('shape'); ?>" style="background-image:url('<?php echo $image['url']; ?>');">
						</div>
					<?php elseif( get_row_layout() == 'galerie_block' ): ?>
						<?php
						$images = get_sub_field('galerie_block__image');
						if( $images ): ?>
							<div class="main-wrapper">
						    <div class="c__carousel">
					        <?php foreach( $images as $image ): ?>
				            <div class="c__carousel__item" style="background-image:url('<?php echo $image['url']; ?>')">
											<img src="" alt="">
			              	<div class="c__carousel__caption"><?php echo $image['caption']; ?></div>
				            </div>
					        <?php endforeach; ?>
					    	</div>
				    	</div>
							<script type="text/javascript">
							jQuery( document ).ready( function( $ ) {
								$(document).ready(function(){
									$('.c__carousel').flickity({
									  // options
									  cellAlign: 'left',
										pageDots: false
									});
								});
							} );
							</script>
						<?php endif; ?>
        	<?php endif;
		    endwhile;
		else :
		    // no layouts found
		endif;

		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
</div>
