<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package origin
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="custom__header" style="background-image:url('<?php echo get_the_post_thumbnail_url($post_id, 'full'); ?>');">
		<div class="main-wrapper">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
		<div class="custom__header__shading"></div>
	</header><!-- .entry-header -->
	<div class="custom__breadcrumbs">
		<div class="main-wrapper">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>

	<div class="page-default entry-content">
		<div class="main-wrapper">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'origin' ),
				'after'  => '</div>',
			) );
		?>
		</div><!-- .entry-content -->
	</div>

</article><!-- #post-## -->
</div>
