<section class="contactbar">
  <div class="main-wrapper">
    <div class="contactbar__col">
      <div class="contactbar__icon">
        <i class="fa fa-mobile fa-2x" aria-hidden="true"></i>
      </div>
      <div class="contactbar__content-wrapper">
        <div class="contactbar__title">
          Appellez-nous
        </div>
        <div class="contactbar__content">
          <?php the_field('telephone', 'option'); ?>
        </div>
      </div>
    </div>
    <div class="contactbar__col">
      <div class="contactbar__icon">
        <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
      </div>
      <div class="contactbar__content-wrapper">
        <div class="contactbar__title">
          Envoyez-nous un message
        </div>
        <div class="contactbar__content">
          <a href="<?php the_field('page_contact', 'option'); ?>">Contact</a>
        </div>
      </div>
    </div>
    <div class="contactbar__col">
      <div class="contactbar__icon">
        <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
      </div>
      <div class="contactbar__content-wrapper">
        <div class="contactbar__title">
          Où nous trouver
        </div>
        <div class="contactbar__content">
          <?php the_field('adresse', 'option'); ?> <br>
          <?php the_field('code_postal', 'option'); ?> <?php the_field('ville', 'option'); ?>
        </div>
      </div>
    </div>
  </div>
</section>
