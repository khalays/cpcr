<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package origin
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="custom__header" style="background-image:url('<?php echo get_the_post_thumbnail_url($post_id, 'full'); ?>');">
		<div class="main-wrapper">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
		<div class="custom__header__shading"></div>
	</header><!-- .entry-header -->
	<div class="custom__breadcrumbs">
		<div class="main-wrapper">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>

	<section class="faq">
		<div class="main-wrapper">
			<?php if( have_rows('faq') ): ?>
			<ul>
			<?php while( have_rows('faq') ): the_row();
				// vars
				$question = get_sub_field('question');
				$reponse = get_sub_field('reponse');
				?>
				<li>
						<a href="javascript:void(0)" class="question expander-trigger expander-hidden"><?php echo $question; ?></a>
						<div class="reponse expander-content">
							<?php echo $reponse; ?>
						</div>
				</li>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
		</div>
	</section><!-- .entry-content -->

</article><!-- #post-## -->
