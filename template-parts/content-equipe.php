<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package origin
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="custom__header" style="background-image:url('<?php echo get_the_post_thumbnail_url($post_id, 'full'); ?>');">
		<div class="main-wrapper">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
		<div class="custom__header__shading"></div>
	</header><!-- .entry-header -->
	<div class="custom__breadcrumbs">
		<div class="main-wrapper">
			<?php custom_breadcrumbs(); ?>
		</div>
	</div>
	<section class="equipe__medecins">
		<div class="main-wrapper">
			<h2>Les médecins</h2>
			<?php if( have_rows('medecins') ): ?>
			<ul>
			<?php $cpt = 0; ?>
			<?php while( have_rows('medecins') ): the_row();
				// vars
				$image = get_sub_field('medecin__image');
				$nom = get_sub_field('medecin__nom');
				$description = get_sub_field('medecin__description');
				?>
				<li class="<?php if ($cpt % 2 != 0) echo "medecin--odd" ?>">
						<div class="medecin__image" style="background-image:url('<?php echo $image['sizes']['large']; ?>')">
						</div>
						<div class="medecin__content">
							<p class="medecin__nom"><strong><?php echo $nom; ?></strong></p>
							<?php echo $description; ?>
						</div>
				</li>
				<?php $cpt++; ?>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
		</div>
	</section>
	<section class="equipe__technique">
		<div class="main-wrapper">
			<?php
			$equipeTechnique = get_field('equipe_technique');
			$equipeSecretariat = get_field('equipe_secretariat');

			 ?>
			<h2>L’équipe technique</h2>
			<h3>Notre équipe technique</h3>
			<img src="<?php echo $equipeTechnique['url'];  ?>" alt="">
			<h3>Notre secrétariat</h3>
			<img src="<?php echo $equipeSecretariat['url'];  ?>" alt="">

		</div>
	</section>
	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'origin' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
