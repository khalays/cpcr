<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package origin
 */

?>
<?php if( have_rows('slider') ): ?>

	<section class="h__carousel">

	<?php while( have_rows('slider') ): the_row();

		// vars
		$image = get_sub_field('slider__image');

		?>
		<div class="carousel__item" style="background-image:url('<?php echo $image; ?>');">
				<div class="carousel__content">
					<div class="main-wrapper">
						<div class="carousel__subtitle"><?php the_sub_field('slider__sous_titre') ?></div>
						<h2><?php the_sub_field('slider__titre') ?></h2>
						<a href="<?php the_sub_field('slider__bouton_lien') ?>" class="btn"><?php the_sub_field('slider__bouton_texte') ?></a>
					</div>
				</div>
			<div class="carousel__shading"></div>
		</div>

	<?php endwhile; ?>

	</section>

<?php endif; ?>

<script type="text/javascript">
jQuery( document ).ready( function( $ ) {
	$(document).ready(function(){
      $('.h__carousel').slick({
				dots: true,
				infinite: true,
				speed: 300,
				slidesToShow: 1,
				fade: true,
				arrows: false,
				autoplay: true,
				autoplaySpeed: 5000
      });
    });
} );
</script>
<?php get_template_part( 'template-parts/contact', 'bar' ); ?>
<article <?php post_class(); ?>>
	<?php $image = get_field('image_1');  ?>
	<div class="h__article__content">
		<h2><?php the_field('titre_1') ?></h2>
		<?php the_field('contenu_1') ?>
	</div>
	<div class="h__article__image" style="background-image:url('<?php echo $image['sizes']['large']; ?>');">
	</div>
</article><!-- #post-## -->
<article <?php post_class('h__article--right'); ?>>
	<?php $image = get_field('image_2');  ?>
	<div class="h__article__image" style="background-image:url('<?php echo $image['sizes']['large']; ?>');">
	</div>
	<div class="h__article__content">
		<h2><?php the_field('titre_2') ?></h2>
		<?php the_field('contenu_2') ?>
	</div>
</article><!-- #post-## -->
