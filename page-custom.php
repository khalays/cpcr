<?php
/**
 * The template for displaying the homepage.
 * Template name: Custom
 *
 * @package smpf
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			get_template_part( 'template-parts/content', 'custom' );
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
