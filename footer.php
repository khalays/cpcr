<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package origin
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer__main">
			<div class="main-wrapper">
				<div class="footer__info">
					<img src="<?php echo get_theme_file_uri( 'assets/img/cpcr-logo-footer.png' ); ?>" alt="Centre de Pathologie Cutanée de la Roquette">
					<p><?php the_field('telephone', 'option'); ?></p>
					<p>
						<?php the_field('adresse', 'option'); ?><br>
						<?php the_field('code_postal', 'option'); ?> <?php the_field('ville', 'option'); ?>
					</p>
				</div>
				<div class="footer__menu">
					<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'footer-menu' ) ); ?>
				</div>
				<div class="footer__pro">
					<ul>
						<li><a href="#" class="footer__pro--button"><i class="fa fa-users fa-2x" aria-hidden="true"></i> Espace Pro</a></li>
						<li><a href="<?php the_field('cgv', 'option'); ?>">Mentions légales</a></li>
					</ul>
				</div>
			</div>
		</div>
			<section class="footer__cred">
				<ul>
					<li>Tous droits réservés Centre de Pathologie Cutanée de la Roquette</li>
					<li><a href="http://www.anamorphik.com/" target="_blank" class="anamorphik-sign"><img src="<?php echo get_theme_file_uri( 'assets/img/k-signature-inline.svg' ); ?>" alt="Anamorphik"></a></li>
				</ul>
			</section>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
